<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;



class LoginModel extends Model{
    //nombre de la tabla
    protected $table = 'usuarios';

    //llave primaria
    protected $primarykey = 'id';
    public $timestamps = false;

    //aqui los elementos a mostrarse en la tabla 
    protected $fillable = [
        'user','password'   ];
}