<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mapper;



class PrincipalController extends Controller
{
    public function index()
    {
      
       /* Mapper::maker(17.067561, -96.693821, ['zoom' => 15, 'center' => false, 'marker' => false, 'type' => 'HYBRID', 'overlay' => 'TRAFFIC']);
        Mapper::maker(16.858816, -96.780049);
        Mapper::maker(16.793976, -96.669437);
        Mapper::maker(15.868094, -97.064491);
        Mapper::maker(15.666189, -96.553604);
        Mapper::maker(15.667439, -96.491337);
        Mapper::maker(17.052337, -96.723583);
        
        Mapper::marker(17.076678, -96.744733);
        Mapper::marker(17.076678, -96.744733);
       // Mapper::map(17.073850, -96.707031)->marker(17.076678, -96.744733, ['markers' => ['symbol' => 'circle', 'scale' => 1000, 'animation' => 'DROP']]);
*/
//Mapper::marker(53.381128999999990000, -1.470085000000040000);
//Mapper::marker(53.381128999999990000, -1.470085000000040000, ['symbol' => 'circle', 'scale' => 1000]);
Mapper::map(17.067561, -96.693821)
->marker(16.858816, -96.780049, ['markers' => ['symbol' => 'circle', 'scale' => 1000, 'animation' => 'DROP']])
 ->marker(16.793976, -96.669437, ['markers' => ['symbol' => 'circle', 'scale' => 1000, 'animation' => 'DROP']])
 ->marker(15.868094, -97.064491, ['markers' => ['symbol' => 'circle', 'scale' => 1000, 'animation' => 'DROP']]) 
 ->marker(15.666189, -96.553604, ['markers' => ['symbol' => 'circle', 'scale' => 1000, 'animation' => 'DROP']]) 
 ->marker(15.667439, -96.491337, ['markers' => ['symbol' => 'circle', 'scale' => 1000, 'animation' => 'DROP']]) 
 ->marker(17.076678, -96.744733, ['markers' => ['symbol' => 'circle', 'scale' => 1000, 'animation' => 'DROP']]) 
 ;


        return view('admin.principal.index', compact('principal')); 
    }

    
  
}