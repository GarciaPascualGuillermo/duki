<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paquetes; // Instanciamos el modelo paquetes 
use Session;
use Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ItemCreateRequest;
use App\Http\Requests\ItemUpdateRequest;
use Illuminate\Support\Facades\Validator;
use DB;
use Input;
use Storage;

class PaqueteController extends Controller
{
    public function index()
    {
    	$paquetes = Paquetes::all();
        return view('admin.paquetes.index', compact('paquetes')); 
    }
    public function create()
    {
        $paquetes = paquetes::all();
        return view('admin.paquetes.create', compact('paquetes'));
    }
    public function edit($id)
    {
        $paquetes = Paquetes::find($id);
        return view('admin/paquetes.edit',['paquetes'=>$paquetes]);
    }

    public function update(ItemUpdateRequest $request, $id)
{        
    $paquetes = Paquetes::find($id);
 
    $paquetes->descripcion = $request->descripcion;
    $paquetes->precio_neto = $request->precio_neto;
    $paquetes->cantidad_folio = $request->cantidad_folio;
 

 
    $paquetes->save();
 
    Session::flash('message', 'Editado Satisfactoriamente !');
    return Redirect::to('admin/paquetes');
}
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ItemCreateRequest $request)
    {
        $paquetes = new paquetes;
 
        $paquetes->descripcion = $request->descripcion;
        $paquetes->precio_neto = $request->precio_neto;
        $paquetes->cantidad_folio = $request->cantidad_folio;
        $paquetes->save();
 
        return redirect('admin/paquetes')->with('message','Guardado Satisfactoriamente !');
    }
    public function destroy($id){
        $imagen = paquetes::find($id);
 
        paquetes::destroy($id);        
 
        Session::flash('message', 'Eliminado Satisfactoriamente !');
        return Redirect::to('admin/paquetes');
    }
}
