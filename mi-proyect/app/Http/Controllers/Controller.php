<?php
 
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\LoginModel;
use Illuminate\Support\Collection;
use App\Http\Models\UsuarioModel;
use DB;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Redirect;
use Session;
use URL;
use App\ventas;
use App\Paquetes;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


public function vistalogin()
  {
        return view('admin.login.login');
  }

  public function vistaregistro()
  {
        return view('admin.login.registro');
  }


public function vista_editar_perfil()
  {
        return view('admin.perfil.perfil');
  }

  public function vista_comprar_paquete()
  {
        return view('admin.perfil.comprar_paquetes');
  }

public function vista_contacto()
  {
        return view('admin.perfil.contacto');
  }


public function vista_carrito()
  {
        return view('admin.perfil.carrito');
  }

  public function vista_sesion()
  {
        return view('admin.perfil.sesion');
  }

  public function vista_paypal()
  {
        return view('paywithpaypal');
  }

public function acceso_login(Request $request)
    {

   
     	
    $usuario = $request->input('username');
    $contra = $request->input('password');
     
    
       
    $id=DB::table('persona')->where('nombre',$usuario)->value('id');
    $user=DB::table('persona')->where('id',$id)->value('nombre');
    $pw=DB::table('persona')->where('id',$id)->value('contrasenia');
    $tipo=DB::table('persona')->where('id',$id)->value('tipo');
   
    if($usuario==$user && $contra==$pw){
    
   if($tipo=='Administrador'){
  
  return view('admin.dashboard.index');
   }if($tipo=='Cliente'){
    return view('admin.perfil.comprar_paquetes');
   }
   
   }  else { 
    return view('admin.login.login'); 
  }

}


public function acceso_directo()
    {

return view('admin.perfil.accesos');

    }



     public function insertarUsuario(Request $datos){
       
       
       
       
       
      
      
       

       $nombre=$datos->input('nombre');
       $ap_paterno=$datos->input('ap_paterno');
       $ap_materno=$datos->input('ap_materno');
       $tipo=$datos->input('tipo');
       $contrasenia=$datos->input('contrasenia');
       $razon_social=$datos->input('razon_social');
       $rfc=$datos->input('rfc');
       $curp=$datos->input('curp');
       $f_nac=$datos->input('f_nac');
       $genero=$datos->input('genero');
       $activo=$datos->input('activo');
       $nombre_repres_legal=$datos->input('nombre_repres_legal');
       $tipo_sociedad=$datos->input('tipo_sociedad');
       $folio_ife=$datos->input('folio_ife');
       $edo_civil=$datos->input('edo_civil');
       $num_tel=$datos->input('Num_tel');
       $sesion=$datos->input('Sesion');
       



DB::table('persona')->insert(array ('Sesion'=>$sesion,'Num_tel'=>$num_tel,'contrasenia'=>$contrasenia,'razon_social'=>$razon_social,'rfc'=>$rfc,'nombre'=>$nombre,'curp'=>$curp,'f_nac'=>$f_nac,'genero'=>$genero,'ap_paterno'=>$ap_paterno,'activo'=>$activo,'ap_materno'=>$ap_materno,'nombre_repres_legal'=>$nombre_repres_legal,'tipo_sociedad'=>$tipo_sociedad,'folio_ife'=>$folio_ife,'edo_civil'=>$edo_civil,'tipo'=>$tipo));




        
        return redirect()->to('registro');
      


        
    }

    public function __construct()
    {
       /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);
    }

 
    public function payWithpaypal(Request $request)
    {
        $payer = new Payer();
            $payer->setPaymentMethod('paypal');
        
            $item_1 = new Item();
            $item_1->setName('Item 1') /** item name **/
                ->setCurrency('USD')
                ->setQuantity(1)
                ->setPrice($request->get('amount')); /** unit price **/
    
            $item_list = new ItemList();
            $item_list->setItems(array($item_1));
    
            $amount = new Amount();
            $amount->setCurrency('USD')
                ->setTotal($request->get('amount'));
    
            $transaction = new Transaction();
            $transaction->setAmount($amount)
                ->setItemList($item_list)
                ->setDescription('Your transaction description');
    
            $redirect_urls = new RedirectUrls();
            $redirect_urls->setReturnUrl(URL::route('status')) /** Specify return URL **/
                ->setCancelUrl(URL::route('status'));
        
            $payment = new Payment();
            $payment->setIntent('Sale')
                ->setPayer($payer)
                ->setRedirectUrls($redirect_urls)
                ->setTransactions(array($transaction));
        /** dd($payment->create($this->_api_context));exit; **/
            try {
                $payment->create($this->_api_context);
            } catch (\PayPal\Exception\PPConnectionException $ex) {
                if (\Config::get('app.debug')) {
                    \Session::put('error', 'Connection timeout');
                    return Redirect::route('paywithpaypal');
                } else {
                    \Session::put('error', 'Some error occur, sorry for inconvenient');
                    return Redirect::route('paywithpaypal');
                }
            }
            foreach ($payment->getLinks() as $link) {
                if ($link->getRel() == 'approval_url') {
                    $redirect_url = $link->getHref();
                    break;
                }
            }
            /** add payment ID to session **/
            Session::put('paypal_payment_id', $payment->getId());
            if (isset($redirect_url)) {
                /** redirect to paypal **/
                return Redirect::away($redirect_url);
            }
            \Session::put('error', 'Unknown error occurred');
            return Redirect::route('paywithpaypal');
    }

    public function getPaymentStatus()
    {
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');

        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {

            \Session::put('error', 'Payment failed');
            return Redirect::to('/');

        }

        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));

        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);

        if ($result->getState() == 'approved') {

            \Session::put('success', 'Payment success');
            return Redirect::to('/');

        }

        \Session::put('error', 'Payment failed');
        return Redirect::to('/');

    }



}


 