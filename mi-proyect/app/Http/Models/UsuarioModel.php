<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;



class UsuarioModel extends Model{
    //nombre de la tabla
    protected $table = 'persona';

    //llave primaria
    protected $primarykey = 'id';
    public $timestamps = false;

    //aqui los elementos a mostrarse en la tabla 
    protected $fillable = ['contrasenia', 'razon_social', 'rfc', 'nombre', 'curp', 'f_nac', 'genero','ap_paterno', 'activo', 'ap_materno', 'nombre_repres_legal', 'tipo_sociedad', 'folio_ife', 'edo_civil', 'tipo', 'num_tel', 'sesion'];

}