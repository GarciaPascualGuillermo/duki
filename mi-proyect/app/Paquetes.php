<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paquetes extends Model
{
    //
    protected $table = 'paquetes';
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;
    protected $fillable = ['descripcion', 'precio_neto', 'cantidad_folio'];
    
}
