


<!DOCTYPE html>
<html lang="en">
<head>
  <title>Registro de usuario</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
    


  </style>
</head>
<body>

<div class="container">
  <h2>Crea una cuenta</h2>
  <form class="form-horizontal" action="{{url('envio')}}" method="post">
    {{ csrf_field() }}
    <div class="form-group">
                  {!! Form::label('nombre', 'Nombre:', array('class' => 'negrita')) !!}                          
                  <div>
                    {!! Form::text('nombre',null,['class'=>'form-control', 'placeholder'=>'Ingrese su nombre', 'required' => 'required']) !!}                              
                  </div>
                </div>

                <div class="form-group">
                  {!! Form::label('ap_paterno', 'Apellido Paterno:', array('class' => 'negrita')) !!}                          
                  <div>
                    {!! Form::text('ap_paterno',null,['class'=>'form-control', 'placeholder'=>'Ingrese su apellido paterno', 'required' => 'required']) !!}                              
                  </div>
                </div>

                <div class="form-group">
                  {!! Form::label('ap_materno', 'Apellido Materno:', array('class' => 'negrita')) !!}                          
                  <div>
                    {!! Form::text('ap_materno',null,['class'=>'form-control', 'placeholder'=>'Ingrese su Apellido Materno', 'required' => 'required']) !!}                              
                  </div>
                </div>

                <div class="form-group">
                    {!! Form::Label('tipo', 'Tipo de cuenta:') !!}
                    <select class="form-control" name="tipo">
                      <option value="Cliente">Cliente</option>
                      </select>
                    </div>

                <div class="form-group">
                  {!! Form::label('contrasenia', 'Contraseña:', array('class' => 'negrita')) !!}                          
                  <div>
                    {!! Form::password('contrasenia',['class'=>'form-control', 'placeholder'=>'Ingrese su contraseña', 'required' => 'required']) !!}                              
                  </div>
                </div>

                <div class="form-group">
                  {!! Form::label('razon_social', 'Razon Social:', array('class' => 'negrita')) !!}                          
                  <div>
                    {!! Form::text('razon_social',null,['class'=>'form-control', 'placeholder'=>'Razon social de la empresa', 'required' => 'required']) !!}                              
                  </div>
                </div>

                <div class="form-group">
                  {!! Form::label('rfc', 'RFC:', array('class' => 'negrita')) !!}                          
                  <div>
                    {!! Form::text('rfc',null,['class'=>'form-control', 'placeholder'=>'Ingrese su RFC', 'required' => 'required']) !!}                              
                  </div>
                </div>

                <div class="form-group">
                  {!! Form::label('curp', 'CURP:', array('class' => 'negrita')) !!}                          
                  <div>
                    {!! Form::text('curp',null,['class'=>'form-control', 'placeholder'=>'Ingrese su CURP', 'required' => 'required']) !!}                              
                  </div>
                </div>

                <div class="form-group">
                  {!! Form::label('f_nac', 'Fecha de Nacimiento:', array('class' => 'negrita')) !!}                          
                  <div>
                    {!! Form::date('f_nac',null,['class'=>'form-control', 'placeholder'=>'Seleccione su Fecha de Nacimiento', 'required' => 'required']) !!}                              
                  </div>
                </div>

                <div class="form-group">
                    {!! Form::Label('genero', 'Sexo:') !!}
                    <select class="form-control" name="genero">
                      <option value="Masculino">Masculino</option>
                      <option value="Femenino">Femenino</option>
                      <option value="Otro">Otro</option>
                      </select>
                    </div>

                    <div class="form-group">
                    {!! Form::Label('activo', 'Estado de cuenta:') !!}
                    <select class="form-control" name="activo">
                      <option value="Activado">Activo</option>
                      </select>
                    </div>

   

                <div class="form-group">
                  {!! Form::label('nombre_repres_legal', 'Nombre del Representante Legal:', array('class' => 'negrita')) !!}                          
                  <div>
                    {!! Form::text('nombre_repres_legal',null,['class'=>'form-control', 'placeholder'=>'', 'required' => 'required']) !!}                              
                  </div>
                </div>

                <div class="form-group">
                    {!! Form::Label('tipo_sociedad', 'Tipo de Sociedad Mercantil:') !!}
                    <select class="form-control" name="tipo_sociedad">
                      <option value="SociedadAnonima">Sociedad Anonima</option>
                      <option value="SociedadCooperativa">Sociedad Cooperativa</option>
                      <option value="SociedadNColectivo">Sociedad de Nombre Colectivo</option>
                      <option value="SociedadResLimitada">Sociedad de Responsabilidad Limitada</option>

                      </select>
                    </div>

                <div class="form-group">
                  {!! Form::label('folio_ife', 'Folio IFE:', array('class' => 'negrita')) !!}                          
                  <div>
                    {!! Form::text('folio_ife',null,['class'=>'form-control', 'placeholder'=>'', 'required' => 'required']) !!}                              
                  </div>
                </div>
                <div class="form-group">
                    {!! Form::Label('edo_civil', 'Estado Civil:') !!}
                    <select class="form-control" name="edo_civil">
                      <option value="Casado">Casado(a)</option>
                      <option value="Divorciado">Divorciado(a)</option>
                      <option value="Soltero">Soltero(a)</option>
                      <option value="Viudo">Viudo(a)</option>

                      </select>
                    </div>
                    <div class="form-group">
                  {!! Form::label('Num_tel', 'Numero Telefonico:', array('class' => 'negrita')) !!}                          
                  <div>
                    {!! Form::text('Num_tel',null,['class'=>'form-control', 'placeholder'=>'', 'required' => 'required']) !!}                              
                  </div>
                </div>

                <div class="form-group">
                    {!! Form::Label('Sesion', 'Sesion:') !!}
                    <select class="form-control" name="Sesion">
                      <option value="0">NA</option>
                      </select>
                    </div>



    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <div class="checkbox">
          <label><input type="checkbox" name="remember"> Recordarme</label>
        </div>
      </div>
    </div>
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default">Crear</button>
        <button type="button" class="btn btn-default" onclick="location.href='{{ url('login') }}'">Login</button>
      </div>
    </div>
   
  </form>

  
</div>

</body>
</html>