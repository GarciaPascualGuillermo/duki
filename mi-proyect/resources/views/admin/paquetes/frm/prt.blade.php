<div class="row">
    <div class="col-md-12">
        <section class="panel">                        
            <div class="panel-body">

                <div class="form-group">
                  {!! Form::label('descripcion', 'descripcion:', array('class' => 'negrita')) !!}                          
                  <div>
                    {!! Form::text('descripcion',null,['class'=>'form-control', 'placeholder'=>'Descripcion del paquete', 'required' => 'required']) !!}                              
                  </div>
                </div>

                <div class="form-group">
                  {!! Form::label('precio_neto', 'precio_neto:', array('class' => 'negrita')) !!}                          
                  <div>
                    {!! Form::text('precio_neto',null,['class'=>'form-control', 'placeholder'=>'numeros enteros', 'required' => 'required']) !!}                              
                  </div>
                </div>

                <div class="form-group">
                  {!! Form::label('cantidad_folio', 'cantidad_folio:', array('class' => 'negrita')) !!}                          
                  <div>
                    {!! Form::text('cantidad_folio',null,['class'=>'form-control', 'placeholder'=>'numeros enteros', 'required' => 'required']) !!}                              
                  </div>
                </div>



                {!! Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-info')) !!}
                {!! link_to(URL::previous(), 'Cancelar', ['class' => 'btn btn-warning']) !!}

                <br><br>
              
            </div>
        </section>
    </div>
</div>