<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Front
// Back
Route::get('/admin/dashboard', 'DashboardController@index')->name('dashboard');
//Route::resource('/admin/postres', 'PostresController');
Route::resource('/admin/paquetes', 'PaqueteController');
Route::resource('/admin/usuarios', 'UsuarioController');
//Route::resource('registro', 'Controller');
Route::get('/inicia','Controller@vistalogin');
Route::post('/submit', 'Controller@acceso_login');
Route::get('registro','Controller@vistaregistro');

Route::get('acceso','Controller@acceso_directo');
Route::get('editar_perfil','Controller@vista_editar_perfil');
Route::get('comprar_paquete','Controller@vista_comprar_paquete');
Route::get('contacto','Controller@vista_contacto');
Route::get('carrito','Controller@vista_carrito');
Route::get('sesion','Controller@vista_sesion');
Route::post('envio', 'Controller@insertarUsuario');

Auth::routes();

Route::get('/home', 'DashboardController@index')->name('home');
Route::get('/', 'PrincipalController@index'); 

Route::get('vista_paypal', 'Controller@vista_paypal')->name('vista_paypal');

// route for processing payment
Route::post('paypal', 'Controller@payWithpaypal')->name('paypal');

// route for check status of the payment
Route::get('status', 'Controller@getPaymentStatus')->name('status');



//enviamos nuestro pedido a paypal
Route::get('payment', array(
    'as'=>'payment',
    'uses'=>'PaypalController@postPayment',
    
    ));
    
    //paypal  redirecciona a esta ruta
    
    Route::get('payment/status', array(
    'as'=>'payment.status',
    'uses'=>'PaypalController@getPaymentStatus',
    
    ));

    Route::get('envioo','PaypalController@capturarid');


   // Route::get('/', 'TelegramBotController@getHome');
Route::get('get-updates', 'TelegramBotController@getUpdates');
Route::get('send',  'TelegramBotController@getSendMessage');
Route::post('send', 'TelegramBotController@postSendMessage');