drop database Duki;
create database Duki;

use Duki;

create table persona (
id int not null auto_increment,
contrasenia varchar(40) not null,
razon_social varchar (40),
rfc varchar (40),
nombre varchar (40),
curp varchar (40),
f_nac date, 
genero varchar (20),
fecha_creacion date,
ap_paterno varchar (40),
class varchar (40),
activo varchar (40),
ap_materno varchar (40),
version varchar (40),
nombre_repres_legal varchar (40),
tipo_sociedad varchar (40),
folio_ife varchar (40),
edo_civil varchar (40),
primary key (id)
);

create table paquete(
id int not null auto_increment ,
descripcion varchar (40),
precio_neto numeric,
cantidad_folio numeric,
primary key (id)
);

Create table empresa_figura(
id int not null auto_increment,
id_persona int references persona(id),
empresa_id numeric,
nombre_corto varchar (40),
pagadora varchar (40),
tipo_operacion varchar(40),
tipo varchar(40),
tipo_persona varchar (40),
activo varchar(40),
version varchar(40),
codigo varchar(40),
primary key (id)
);

create table impuesto(
id  int not null auto_increment,
iva numeric,
primary key (id)
);


create table venta(
id  int not null auto_increment,
fecha date,
total numeric,
id_impuesto int,
id_paquete int,
id_emp_fig int,
primary key (id),
foreign key (id_impuesto) references impuesto(id),
foreign key (id_paquete)  references paquete(id),
foreign key (id_emp_fig)  references empresa_figura(id)
);

create table pago(
id int not null auto_increment,
id_venta int not null,
comentario varchar (40),
fecha date,
status varchar (40),
primary key (id),
foreign key (id_venta) references venta(id)
);